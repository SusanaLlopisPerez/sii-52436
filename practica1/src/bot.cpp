//Autora:Susana Llopis Pérez

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "DatosMemCompartida.h"	


int main(int argc, char *argv[])
{
	DatosMemCompartida *datos;
	int fd=open("OBJETOS.txt",O_RDWR);
        if (fd<0) {
           perror("Error apertura fichero");
           exit(1);
        }

	//Proyeccion en memoria
	if ((datos=(DatosMemCompartida *)mmap(0, sizeof(DatosMemCompartida), PROT_WRITE|PROT_READ, MAP_SHARED,fd, 0))==MAP_FAILED){
                perror("Error en la proyeccion del fichero");
                close(fd);
                return(1);
	}
        close(fd);

	while(1){
		datos->accion=0;
		if(datos->raqueta1.y2<datos->esfera.centro.y) datos->accion=1;
		if(datos->raqueta1.y2>datos->esfera.centro.y) datos->accion=-1;
		usleep(25000);
	}
	
return (0);
		
}
