#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "Esfera.h"
#include "Raqueta.h"
#include "Socket.h"
#include <pthread.h>
#include <sys/types.h>          
#include <sys/socket.h>


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	int tuberia;

	//Thread
	pthread_t thid1;

	void RecibeComandosJugador();
	//Socket
	struct sockaddr_in s_ain, c_ain;
	int sd,cd;
	

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
