//Autora: Susana Llopis Pérez

#pragma once
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida
{
public:
	Esfera esfera;
	Raqueta raqueta1;
	int accion; //1arriba, 0nada, -1abajo
};
